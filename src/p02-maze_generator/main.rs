// Game Engine
use olc_pixel_game_engine as olc;

use rand::prelude::ThreadRng;
use rand::Rng;

use prototypes::tools::Stack;

// === Constant Definitions === //
const MAZE_W: usize = 30;
const MAZE_H: usize = 25;

const TILE: i32 = 16;

const SCREEN_W: i32 = MAZE_W as i32 * TILE + 2;
const SCREEN_H: i32 = MAZE_H as i32 * TILE + 2;

const DIRECTIONS: [olc::Vi2d; 4] = [
    olc::Vi2d { x:  0, y: -1 },
    olc::Vi2d { x:  0, y:  1 },
    olc::Vi2d { x:  1, y:  0 },
    olc::Vi2d { x: -1, y:  0 }
];

const DELAY: f32 = 0.01; // Ten milli seconds
// ============================ //

#[derive(Default)]
struct MazeMapGrid {
    already_visited: [[bool; MAZE_H]; MAZE_W],
    visited_counter: u32
}

impl MazeMapGrid {
    fn valid_position(&mut self, spot: &olc::Vi2d) -> bool {
        (spot.x >= 0 && spot.x < MAZE_W as i32) && (spot.y >= 0 && spot.y < MAZE_H as i32) &&
            !self.already_visited[spot.x as usize][spot.y as usize]
    }
    fn is_full(&self) -> bool {
        self.visited_counter >= (MAZE_W * MAZE_H) as u32
    }
    fn register_new_visited(&mut self, spot: &olc::Vi2d) {
        if self.valid_position(spot) {
            self.already_visited[spot.x as usize][spot.y as usize] = true;
            self.visited_counter += 1;
        }
    }
}

#[derive(Default)]
struct MazeGenerator {
    map: MazeMapGrid,
    wait_duration: f32,
    stack: Stack<olc::Vi2d>,
    rng: ThreadRng
}

impl MazeGenerator {
    fn draw_square(&mut self, x: i32, y: i32) {
        let px: i32 = x * TILE + 1;
        let py: i32 = y * TILE + 1;
        for x_offset in 1..(TILE - 1) {
            for y_offset in 1..(TILE - 1) {
                olc::draw(px + x_offset, py + y_offset, olc::BLUE);
            }
        }
    }

    fn fill_path(&mut self, p1: &olc::Vi2d, p2: &olc::Vi2d) {
        let px: i32; let py: i32;
        let width: i32; let height: i32;
        if p1.x == p2.x {
            width = TILE - 1; height = 2 * TILE - 1;
            if p1.y < p2.y {
                px = p1.x * TILE + 1;
                py = p1.y * TILE + 1;
            }
            else {
                px = p2.x * TILE + 1;
                py = p2.y * TILE + 1;
            }
        }
        else {
            width = 2 * TILE - 1; height = TILE - 1;
            if p1.x < p2.x {
                px = p1.x * TILE + 1;
                py = p1.y * TILE + 1;
            }
            else {
                px = p2.x * TILE + 1;
                py = p2.y * TILE + 1;
            }
        }
        olc::fill_rect(px, py, width, height, olc::WHITE);
    }
}

impl olc::Application for MazeGenerator {
    fn on_user_create(&mut self) -> Result<(), olc::Error> {

        olc::fill_rect(0, 0, SCREEN_W, SCREEN_H, olc::BLACK);
        for x in 0..MAZE_W {
            for y in 0..MAZE_H {
                self.draw_square(x as i32, y as i32);
            }
        }

        self.map = MazeMapGrid { ..Default::default() };
        self.map.register_new_visited(&olc::Vi2d { x: 0, y: 0 });

        self.stack.push(olc::Vi2d { x: 0, y: 0 });
        self.wait_duration = 0.0;

        self.rng = rand::thread_rng();

        Ok(())
    }

    fn on_user_update(&mut self, elapsed_time: f32) -> Result<(), olc::Error> {

        self.wait_duration += elapsed_time;
        if self.wait_duration < DELAY { // Do nothing until the delay is finished
            return Ok(())
        }
        self.wait_duration -= DELAY;

        if self.stack.is_empty() {
            panic!("Empty Stack!! WTF???");
        }

        if !self.map.is_full() {
            let mut not_visited_neighours: Vec<olc::Vi2d> = vec![];

            let top: olc::Vi2d = *self.stack.top();

            for dir in DIRECTIONS {
                let neighbour = top + dir;
                if self.map.valid_position(&neighbour) {
                    not_visited_neighours.push(neighbour);
                }
            }

            if not_visited_neighours.is_empty() {
                self.stack.pop();
            }
            else {
                let chosen_index: usize = self.rng.gen_range(0..not_visited_neighours.len());
                let neighbour = &(not_visited_neighours[chosen_index]);

                self.map.register_new_visited(neighbour);
                self.stack.push(olc::Vi2d { ..*(neighbour) });

                self.fill_path(&top, neighbour);
            }
        }

        Ok(())
    }

    fn on_user_destroy(&mut self) -> Result<(), olc::Error> {

        Ok(())
    }
}

fn main() {
    let mut app = MazeGenerator { ..Default::default() };
    olc::start("Maze Generator", &mut app, SCREEN_W, SCREEN_H, 2, 2).unwrap();
}
