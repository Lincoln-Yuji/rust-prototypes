// Pixel Game Engine
use olc_pixel_game_engine as olc;

const SCREEN_W: i32 = 1200;
const SCREEN_H: i32 =  800;

const MAX_ITER: i32 = 150;

#[derive(Default)]
struct Mandelbrot {
    pos0: olc::Vf2d,
    pos1: olc::Vf2d,
    selecting: bool, first_mx: i32, first_my: i32
}

impl Mandelbrot {
    fn map_to_complex(&mut self, px: i32, py: i32) -> olc::Vf2d {
        let out_x: f32 = (self.pos1.x - self.pos0.x) * (px as f32 / SCREEN_W as f32) + self.pos0.x;
        let out_y: f32 = (self.pos1.y - self.pos0.y) * (py as f32 / SCREEN_H as f32) + self.pos0.y;
        return olc::Vf2d { x: out_x, y: out_y }
    }

    fn compute_degree(&mut self, c: olc::Vf2d) -> i32 {
        let mut z = olc::Vf2d { x: 0.0, y: 0.0 };
        let mut n = 0;
        while z.mag2() <= 4.0 && n < MAX_ITER {
            // z = z * z + c
            z = olc::Vf2d { x: z.x * z.x - z.y * z.y, y: 2.0 * z.x * z.y } + c;
            n += 1;
        }
        return n
    }

    fn draw_mandelbrot(&mut self) {
        for px in 0..SCREEN_W {
            for py in 0..SCREEN_H {
                let mapped_point: olc::Vf2d = self.map_to_complex(px, py);
                let iterations: i32 = self.compute_degree(mapped_point);

                let intensity: u8 = 255 - (iterations as f32 * 255.0 / MAX_ITER as f32) as u8;

                olc::draw(px, py, olc::Pixel { r: intensity, g: intensity, b: intensity, a: 255 });
            }
        }
    }
}

impl olc::Application for Mandelbrot {
    fn on_user_create(&mut self) -> Result<(), olc::Error> {
        self.draw_mandelbrot();
        Ok(())
    }

    fn on_user_update(&mut self, _elapsed_time: f32) -> Result<(), olc::Error> {
        // Zoom in the mandelbrot set
        if olc::get_mouse(0).pressed {
            println!("[ STATUS ] Selecting...");
            self.first_mx = olc::get_mouse_x();
            self.first_my = olc::get_mouse_y();
            self.selecting = true;
        }
        if olc::get_mouse(0).released && self.selecting {
            println!("[ STATUS ] Selected!");
            let mx: i32 = olc::get_mouse_x();
            let my: i32 = olc::get_mouse_y();
            self.selecting = false;

            self.pos0 = self.map_to_complex(self.first_mx, self.first_my);
            self.pos1 = self.map_to_complex(mx, my);

            self.draw_mandelbrot();
        }
        Ok(())
    }

    fn on_user_destroy(&mut self) -> Result<(), olc::Error> {
        Ok(())
    }
}

fn main() {
    let mut app: Mandelbrot = Mandelbrot {
        pos0: olc::Vf2d{x: -2.0, y: -1.0},
        pos1: olc::Vf2d{x:  1.0, y:  1.0},
        ..Default::default()
    };
    olc::start("Mandelbrot Set", &mut app, SCREEN_W, SCREEN_H, 1, 1)
        .unwrap_or_else(|err_msg| {
            panic!("Problem starting the application: {:?}", err_msg);
    });
}
