pub mod tools {
    use std::collections::LinkedList;

    #[derive(Default)]
    pub struct Stack<T> {
        pseudo_stack: LinkedList<T>
    }

    impl<T> Stack<T> {
        pub fn push(&mut self, x: T) {
            self.pseudo_stack.push_back(x);
        }
        pub fn pop(&mut self) {
            self.pseudo_stack.pop_back();
        }
        pub fn top(&mut self) -> &T {
            self.pseudo_stack.back().unwrap()
        }
        pub fn is_empty(&mut self) -> bool {
            self.pseudo_stack.is_empty()
        }
    }
}
