// Pixel Game Engine
use olc_pixel_game_engine as olc;

use rand::prelude::ThreadRng;
use rand::Rng;

use std::{thread, time};

const SCREEN_W: i32 = 1200;
const SCREEN_H: i32 = 800;

const MAX: usize = 600;

static mut APP: VisualMergeSort = VisualMergeSort { arr: [0; MAX] };

struct VisualMergeSort {
    arr: [i32; MAX],
}

impl VisualMergeSort {
    fn shuffle(&mut self) {
        let mut rng: ThreadRng = rand::thread_rng();
        for i in 0..MAX {
            let j: usize = rng.gen_range(0..MAX);
            // swap
            let aux = self.arr[i]; 
            self.arr[i] = self.arr[j];
            self.arr[j] = aux;
        }
    }
    
    fn merge_sort(&mut self) {
        let mut tmp = [0; MAX];
        self.merge_sort_aux(&mut tmp, 0, MAX - 1);
    }

    fn merge_sort_aux(&mut self, tmp: &mut [i32; MAX], start: usize, end: usize) {
        if start < end {
            let mid: usize = (start + end) / 2;

            self.merge_sort_aux(tmp, start, mid);   // Left
            self.merge_sort_aux(tmp, mid + 1, end); // Right

            let mut i: usize = start;
            let mut j: usize = mid + 1;
            let mut k: usize = start;

            while i <= mid && j <= end {
                if self.arr[i] < self.arr[j] {
                    tmp[{ k+=1; k-1 }] = self.arr[{i+=1; i-1}];
                }
                else {
                    tmp[{ k+=1; k-1 }] = self.arr[{j+=1; j-1}];
                }
            }

            while i <= mid {
                tmp[{ k+=1; k-1 }] = self.arr[{i+=1; i-1}];
            }
            while j <= end {
                tmp[{ k+=1; k-1 }] = self.arr[{j+=1; j-1}];
            }

            for t in start..(end + 1) {
                self.arr[t] = tmp[t];
                thread::sleep(time::Duration::from_millis(10));
            }
        }
    }
}

impl olc::Application for VisualMergeSort {
    fn on_user_create(&mut self) -> Result<(), olc::Error> {

        olc::fill_rect(0, 0, SCREEN_W, SCREEN_H, olc::BLACK);
        Ok(())
    }

    fn on_user_update(&mut self, _elapsed_time: f32) -> Result<(), olc::Error> {
        olc::fill_rect(0, 0, SCREEN_W, SCREEN_H, olc::BLACK);

        let w: i32 = SCREEN_W / MAX as i32;
        for i in 0..MAX {
            let h: i32 = (self.arr[i] * SCREEN_H) / MAX as i32;
            olc::fill_rect(i as i32 * w, SCREEN_H - h, w, h, olc::WHITE);
        }

        Ok(())
    }

    fn on_user_destroy(&mut self) -> Result<(), olc::Error> {
        Ok(())
    }
}

fn main() {
    unsafe {
        for i in 0..MAX {
            APP.arr[i] = i as i32;
        }
        APP.shuffle();

        thread::spawn(|| { APP.merge_sort(); });
        olc::start("Merge Sort", &mut APP, SCREEN_W, SCREEN_H, 1, 1).unwrap();
    }
}
