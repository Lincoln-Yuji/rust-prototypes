use olc_pixel_game_engine as olc;
use rand::prelude::ThreadRng;
use rand::Rng;

const RADIUS: i32 = 530;
const SCREEN_DIM: i32 = 1080;
const BORDER: i32 = 10;

const NULL: () = ();

#[derive(Default)]
struct MonteCarlo {
    dots_in:    u128,
    dots_total: u128,
    rng: ThreadRng
}

impl olc::Application for MonteCarlo {
    fn on_user_create(&mut self) -> Result<(), olc::Error> {
        self.dots_in    = 0; 
        self.dots_total = 0;
        self.rng = rand::thread_rng();

        olc::draw_circle(SCREEN_DIM / 2, SCREEN_DIM / 2, RADIUS, olc::WHITE);
        olc::draw_rect(BORDER, BORDER, 2 * RADIUS, 2 * RADIUS, olc::WHITE);

        Ok(NULL)
    }

    fn on_user_update(&mut self, _elapsed_time: f32) -> Result<(), olc::Error> {
        if self.dots_total == u128::MAX {
            return Ok(NULL)
        }

        let x: f32 = self.rng.gen_range(BORDER as f32..(SCREEN_DIM - BORDER) as f32);
        let y: f32 = self.rng.gen_range(BORDER as f32..(SCREEN_DIM - BORDER) as f32);
        let c: f32 = (SCREEN_DIM / 2) as f32;

        let dist_from_center_squared: f32 = (x - c) * (x - c) + (y - c) * (y - c);

        if dist_from_center_squared < (RADIUS * RADIUS) as f32 {
            olc::fill_circle(x as i32, y as i32, 1, olc::GREEN);
            self.dots_in += 1;
        }
        self.dots_total += 1;

        let estimated_pi: f64 = (4.0 * (self.dots_in as f64)) / self.dots_total as f64;
        olc::fill_rect(20, 20, 150, 50, olc::BLACK);
        olc::draw_string(20, 20, &estimated_pi.to_string(), olc::WHITE).unwrap();

        Ok(NULL)
    }

    fn on_user_destroy(&mut self) -> Result<(), olc::Error> {
        Ok(NULL)
    }
}

fn main() {
    let mut application = MonteCarlo { ..Default::default() };
    olc::start("Monte Carlo", &mut application, SCREEN_DIM, SCREEN_DIM, 1, 1).unwrap();
}

