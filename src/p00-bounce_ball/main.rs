use olc_pixel_game_engine as olc;

struct Ball {
    radius: f32,
    pos_x:  f32, pos_y:  f32,
    vel_x:  f32, vel_y:  f32,
    color:  olc::Pixel
}

impl Ball {
    pub fn update(&mut self, delta: f32) {
        let screen_w: f32 = olc::screen_width() as f32;
        let screen_h: f32 = olc::screen_height() as f32;

        let future_x: f32 = self.pos_x + self.vel_x * delta;
        let future_y: f32 = self.pos_y + self.vel_y * delta;

        if (future_x + self.radius > screen_w) || (future_x - self.radius < 0.0) {
            self.vel_x *= -1.0;
        }
        if (future_y + self.radius > screen_h) || (future_y - self.radius < 0.0) {
            self.vel_y *= -1.0;
        }

        self.pos_x += self.vel_x * delta;
        self.pos_y += self.vel_y * delta;
    }
    pub fn render(&mut self) {
        olc::fill_circle(self.pos_x as i32, self.pos_y as i32, self.radius as i32, self.color);
    }
}

struct MyApp {
    ball: Ball
}

impl olc::Application for MyApp {
    fn on_user_create(&mut self) -> Result<(), olc::Error> {

        Ok(())
    }

    fn on_user_update(&mut self, elapsed_time: f32) -> Result<(), olc::Error> {
        self.ball.update(elapsed_time);

        olc::clear(olc::BLACK);
        self.ball.render();

        Ok(())
    }

    fn on_user_destroy(&mut self) -> Result<(), olc::Error> {

        Ok(())
    }
}

fn main() {
    let white_ball = Ball {
        radius: 40.0,
        pos_x: 500.0, pos_y: 500.0,
        vel_x: 400.0, vel_y: 300.0,
        color: olc::WHITE
    };
    let mut myapp = MyApp { ball: white_ball };
    olc::start("Bounce-Ball", &mut myapp, 800, 800, 1, 1).unwrap();
}
