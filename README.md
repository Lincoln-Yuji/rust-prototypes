# Prototype projects made in Rust

This is a collection of simple and small prototype projects made in Rust.
Prototyping is good when we are starting to learn a new language and want to know more
libraries.

# Optional crates for drawing in 2D

We need to integrate some rust crates onto our project that will helps us to do basic tasks
such us drawing (inside a controlled loop) and to make geometry handling (operations
over vectors and matrices, linear transformations, etc) without having to either reinvent
the wheel or build a half assed library ourselves.

I listed some options that I had in mind and my definitive choice.

## Option 1: Macroquad

A library you can use to draw and render 2D stuff on screen is the
[macroquad](https://github.com/not-fl3/macroquad) rust crate.

However, it's important to note that Macroquad has a very incovenient standard behviour.
The screen buffer is cleared everytime we call the next frame function:

```rs
-- snip --
macroquad::prelude::next_frame().await
```

It's really important to call this function at the end of every loop, beceuse that's exactly
what keeps the frame rate stable and you will get several errors if you try to implement the
loop without it.

When we call functions such as draw\_cricle, we are not actually drawing on the screen. What
we are doing is writting stuff onto a buffer. The rendering process occurs when the next frame
function is called. This buffer is cleared by default everytime the rendering step ends and
there is no tweak we can do to change such behaviour.

Due to those incoveniences, doing some stuff like simulating Monte Carlo or drawing a Mandelbrot
set gets simply not viable at all.

For these cases, we have to use an alternative. But if the buffer refresh is a desired behaviour
then macroquad is a really good crate to choose.

## Option 2: Nannou

[Nannou](https://github.com/nannou-org/nannou) is a viable alternative when we don't want the 
screen to be refreshed every single frame. It's a very interesting crate for those looking up
for a creative conding library.

But keep in mind the fact that nannou does not have a native way to read input event from the
keyboard or mouse. You will have to add other extensions to your project if you see the
necessity.

Nannou has a rather good and beginner friendly [manual](https://guide.nannou.cc/welcome.html).
It's worth check it out.

## Option 3: Raw Framebuffer library

If you decide to use some kind of library that only provides a framebuffer and primitive functions
to draw on it, you will probably have to implement the main loop yourself.
An example of how to get this, in rust, is shown below:

```rs
let polar: f64 = 60.0 / 1000000000.0;
let mut checkpoint = std::time::Instant::now();
let mut delta: f64 = 0.0;

loop {
    let elapsed_time: f64 = checkpoint.elapsed().as_nanos() as f64;
    delta += elapsed_time * polar;
    checkpoint = std::time::Instant::now();
    if delta >= 1.0 {

        < ... CODE ... >

        delta -= 1.0;
    }
}
```

This technique is very interesting since it creates a loop whose rate will be clamped with a
maximum of 60 fps. The coolest part is that this algorithm is very easy to reproduce on
different languages, making it not restrained to Rust.

Note that this template is indeed simple and more sofisticated API's will implement this loop
using other ways.

## Option 4: Pixel Game Engine
This is the chosen one. This library was originally written in C++ and developed by
[javidx9](https://www.youtube.com/c/javidx9) and its contributors. The
[olcPixelGameEngine](https://github.com/OneLoneCoder/olcPixelGameEngine) balances simplicity
with functionality and it's a very light game engine.

The engine has a [port for Rust](https://github.com/sadikovi/olcPixelGameEngine-rs).
Some people won't like the fact this library isn't almost entirely written in Rust and
has a core whose code still relies too much on C++. Particularly I don't mind it. I don't
need to write C++ code to use the crate and I don't hate the language either.

But if you do want to use a more "pure Rust" crate, ggez and coffee are interesting game engines
to try. If you are interested on heavily developing games and not just prototypes, I would
recommend a more robust game engine such as [Bevy](https://bevyengine.org/).

The Pixel Game Engine doesn't have the flaws that made me not choose Macroquad or Nannou.
But it's important to note that the engine is not perfect either. I wouldn't recommend this
engine if you are heavily focused on creative coding or animation. There are better crates for
that out there for those tasks. It's good enough to my personal use.

External [dependencies](https://github.com/OneLoneCoder/olcPixelGameEngine/wiki/Compiling-on-Linux)
required by olcPixelGameEngine are listed on the official repository. On Arch Linux we need:

```sh
# gcc not required since we are compiling a rust program, but it's the general compiler 
# for C programs in any GNU/Linux distribution.
$ sudo pacman -S gcc glibc libpng mesa --needed
```
